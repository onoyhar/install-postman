#!/bin/bash
echo "Download Postman On Progressss..."
wget https://dl.pstmn.io/download/latest/linux64 -O postman-linux-x64.tar.gz
echo "Extract Postman to /opt..."
sudo tar xvzf postman-linux-x64.tar.gz -C /opt
echo "Create Symbolic Link Postman to /usr/bin/postman..."
sudo ln -s /opt/Postman/Postman /usr/bin/postman


echo "Create Desktop Shortcut Postman..."
cat << EOF > ~/.local/share/applications/postman2.desktop
[Desktop Entry]
Name=Postman
GenericName=API Client
X-GNOME-FullName=Postman API Client
Comment=Make and view REST API calls and responses
Keywords=api;
Exec=/opt/Postman/Postman
Terminal=false
Type=Application
Icon=/opt/Postman/app/resources/app/assets/icon.png
Categories=Development;Utilities;
EOF


echo "Install Postman Successfully.."
echo "Thanks!!!"
.
